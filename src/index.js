import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import * as serviceWorker from "./serviceWorker";
import User from "./models/user";
import { Provider } from "mobx-react";
import { connectReduxDevtools } from "mst-middlewares";
import makeInspectable from 'mobx-devtools-mst'
import { onAction, types } from "mobx-state-tree";
import { CSSReset, ThemeProvider } from "@chakra-ui/core";
import NavBar from "./components/organisms/Navbar";
import { BrowserRouter } from "react-router-dom";
import Routes from "./Routes";
import Cookies from 'js-cookie'
import { UserPlaylists } from "./models/playlists";
import { PlaylistVideos } from "./models/videos";

export const rootStore = types.compose(
  User,
  UserPlaylists,
  PlaylistVideos
).named("Store");

let store = rootStore.create({
  isAuthorized: Cookies.get("token") !== undefined
});

onAction(store, (call) => {
  console.log(call);
 }); // middleware
makeInspectable(store)
connectReduxDevtools(require("remotedev"), store);
ReactDOM.render(
  <React.StrictMode>
    <ThemeProvider>
      <CSSReset />
      <Provider store={store}>
        <BrowserRouter>
          <NavBar />

          <Routes/>
        </BrowserRouter>
      </Provider>
    </ThemeProvider>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
