import React from "react";
import { Route, Switch } from "react-router-dom";
import App from "./App";
import AddPlaylist from "./components/organisms/AddPlaylist";
import UploadVideo from "./components/organisms/UploadVideo";
import UserPlaylistsLayout from "./containers/UserPlaylistsLayout";
import ViewVideoLayout from "./containers/ViewVideoLayout";

/* Routes setup */

const Routes = () => {
  return (
    <Switch>
      <Route exact path="/" component={App} />

      <Route path="/view" component={ViewVideoLayout} />

      <Route path="/add-playlist" component={AddPlaylist} />

      <Route path="/upload-video/:id" component={UploadVideo} />

      <Route path="/my-playlists" component={UserPlaylistsLayout} />
    </Switch>
  );
};

export default Routes;
