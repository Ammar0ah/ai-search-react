import React from 'react';
import './App.css';
import { observer } from 'mobx-react';
import { Box } from '@chakra-ui/core';
import HomeLayout from './containers/HomeLayout'

/** root */

function App() {

 
  return  (
      <Box p={"1em"} px={"1em"} mt="4em" className=".App">
        <HomeLayout />
      </Box>
  );
}

export default observer(App);
