import { values } from "lodash";
import axios from "../config/axios";
const { flow } = require("mobx-state-tree");
const { default: User } = require("./user");

/** User Playlist actions */

export const UserPlaylists = User.actions((self) => ({
  addPlaylist: flow(function* (payload, onSuccess = null, onFail = null) {
    try {
      const res = yield axios.post("/user/add/playlist", payload, {
        "Content-Type": "multipart/form-data",
      });
      if (res.status === 200) {
        self.state = "success";
        self.message = "Playlist added successfully";
        self.playlists = res.data.playlists;
        onSuccess && onSuccess();
      }
    } catch (err) {
      self.state = "error";
      self.message = "Please fill all fields";
     
      onFail();
    }
  }),

  getMyPlaylist: flow(function* (action = null) {
    try {
      const res = yield axios.get("/user/show/playlists");
      const { playlists } = res.data;
      self.playlists = playlists;
      if (res.status === 201) {
        self.state = "success";
        playlists &&
          playlists.forEach((pl, index) => {
            pl.video_count > 0 && self.getVideos(pl.id, index);
          });
      } else if (res.status === 401) {
        self.state = "error";
        self.message = "Session timeout, please login again";
        action && action();
        self.getHomePlaylist();
      }
    } catch (error) {
      self.state = "error";
      self.message = "Server Error";
      action && action();
    }
  }),

  getHomePlaylist: flow(function* (action = null) {
    try {
      const res = yield axios.get("/index");
      self.defaultPlaylist = values(res.data);
      self.state = "success";

      action && action();
    } catch (error) {
      self.message = "Server Error";
      self.state = "error";
      action && action();
    }
  }),
}));
