import cookie from "js-cookie";
import {
  types,
  flow,
  getSnapshot,
  applySnapshot,
  onSnapshot,
} from "mobx-state-tree";
import axios, { setTokenHeader } from "../config/axios";
import moment from "moment";

let initialState = {};

const User = types
  .model("User", {
    id: types.optional(types.number, -1),
    name: types.optional(types.string, ""),
    email: types.optional(types.string, ""),
    created_at: types.optional(types.string, ""),
    user_picture: types.optional(types.string, ""),
    state: types.optional(
      types.enumeration("state", ["init", "loading", "success", "error"]),
      "init"
    ),
    message: types.optional(types.string, ""),
    isAuthorized: types.optional(types.boolean, false),
    playlists: types.optional(types.frozen(), []),
    defaultPlaylist: types.optional(types.frozen(), []),
    isHome:types.optional(types.boolean, false),
  })
  .actions((self) => ({
    reqAuth: flow(function* (
      route,
      data,
      onSuccess = null,
      onFail = null,
      action = null
    ) {
      try {
        initialState = getSnapshot(self);

        const res = yield axios.post(`/auth/${route}`, data);
        const {
          access_token,
          message,
          user,
          token_type,
          expires_at,
        } = res.data;

        if (res.status === 200 || 201) {
          cookie.set("user", user);
          self.isAuthorized = true;
          self.message = message;
          self.state = "success";
          self.name = user.name;
          self.id = user.id;
          self.email = user.email;
          self.created_at = user.created_at;
          onSuccess && onSuccess();
          setTokenHeader(access_token);
          cookie.set("token", access_token, {
            expires: moment(expires_at).toDate(),
          });
          cookie.set("tokenType", token_type, {
            expires: moment(expires_at).toDate(),
          });
          window.location.reload()
        } else {
          onFail && onFail();
          self.isAuthorized = false;
          self.message = message;
        }
        action();
      } catch (error) {
        self.isAuthorized = false;

        self.state = "error";
        if (error.response?.data) {
          self.message =
            error?.response?.data?.errors &&
            error?.response?.data.errors
              .map((err) => Object.values(err))
              .join(", ");
          self.state = "error";

          onFail && onFail();
        } else {
          self.state = "error";

          self.message = "Server Error";
        }
        action();
      }
    }),

    logout: () => {
      self.isAuthorized = false;
      setTokenHeader(null);
      cookie.remove("token");
      cookie.remove("tokenType");
      applySnapshot(self, initialState);
      self.isAuthorized = false;
    },

    getUser: flow(function* (onSuccess = null, onFail = null) {
      try {
        const token = cookie.get("token");
        if (token) {
          const res = yield axios.get("/user/info");
          if (res.status === 200) {
            let isHome = self.isHome
            applySnapshot(self, res.data.user);
            cookie.set("isHome",true)
            self.isHome = isHome
            self.isAuthorized = true;
            self.state = "success";
          } else if (res.status === 401) {
            self.state = "error";
            self.message = "Session timeout, please login again";
            onFail();
            self.getHomePlaylist();
          }
        } else {
          self.isAuthorized = false;
        }
      } catch (error) {
        self.state = "error";
        self.message = "Server Error";
        onFail();
      }
    }),

    setState: (state) => {
      self.state = state;
    },

    setHome:(isHome) => {
      cookie.set("isHome",isHome)
      localStorage.setItem("isHome",isHome)
      self.isHome = isHome
    },
    afterCreate() {
      onSnapshot(self, (snapshot) => console.log("snapShot", snapshot));
    },
  }));

export default User;
