import cookie from "js-cookie";
import { isObject, values } from "lodash";
import axios from "../config/axios";
const { flow } = require("mobx-state-tree");
const { default: User } = require("./user");

/** User single playlist video actions */

export const PlaylistVideos = User.actions((self) => ({
  getVideos: flow(function* (vid, id) {
    const res = yield axios.get(`/playlist/videos/${vid}`);
    const { video } = res.data;
    let videos = values(video.videos);
    if (self.playlists[id]) {
      let modified = {
        ...self.playlists[id],
        name: self.playlists[id].name,
        videos,
      };
      let tempPlaylist = self.playlists.filter((playlist) => !!playlist);
      tempPlaylist[id] = { ...modified };
      self.playlists = tempPlaylist;
      modified = {};
    }
  }),

  getTopics: (pl_ind, v_id) => {
    console.log('pl ind',pl_ind,v_id,self.playlists[pl_ind],cookie.get("isHome"))
    if (self.isHome) {
      console.log('in video home')
      const video = self.defaultPlaylist.find(
        (video) => isObject(video) && video.id + "" === v_id
      );
      console.log("VV",video)
      // console.log("topics",video.topics[0].topics)
      if (video.topics) {
        return video.topics.topics;
      }
    } else {
      
      console.log('in user playlist')

      const playlist = self.playlists[pl_ind];
      console.log(playlist,pl_ind)
      const video = playlist && playlist.videos &&  playlist.videos.find(
        (video) =>  {
          console.log(video)
          return video.id + "" === v_id && video}
        );
        console.log(video)

      if (video && video.pre_processing === 0) {
        console.log(video)
        return video?.topics?.topics;
      }
    }
    
  },
}));
