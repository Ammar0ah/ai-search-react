import axios from "axios";
import cookie from "js-cookie";

const instance = axios.create({
  baseURL: "http://192.168.1.103:8000/api",
  headers: { Authorization: `Bearer ${cookie.get("token")}` },
});

instance.interceptors.request.use((req) => {
  if (process.env.NODE_ENV !== "production")
    console.log("%c ---Starting Request", "color: blue", req);
  return req;
});

instance.interceptors.response.use(
  (res) => {
    if (process.env.NODE_ENV !== "production")
      console.log("%c ---Ending Response", "color: green", res);
    return res;
  },
  (err) => {
    if (process.env.NODE_ENV !== "production")
      console.log("%c ---Error Response", "color: red", err.response);
    return Promise.reject(err);
  }
);

instance.defaults.headers.common["Content-Type"] = "multipart/form-data";
// instance.defaults.headers.common['Accept-Encoding'] = 'gzip, deflate, br';
instance.defaults.headers.common.Accept = "application/json";
instance.defaults.headers.common["Accept-Language"] = "en";
instance.defaults.headers.common["Access-Control-Allow-Origin"] = "*";
export const setTokenHeader = (token) => {
  instance.defaults.headers.common.Authorization = `Bearer ${token}`;
};

export default instance;
