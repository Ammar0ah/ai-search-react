import React from "react";
import { Box, Text } from "@chakra-ui/core";
import SummaryCard from "../../atoms/SummaryCard";

const VideoTopics = ({ topics, seek,id }) => {
  console.log(topics)
  return (
    <Box>
      <Box
        display="flex"
        justifyContent="flex-start"
        borderBottom="#838383 1px solid"
        textAlign="center"
      >
        <Text fontSize="24px" fontWeight="bold"  flex={1}>
          Headline
        </Text>
        <Text fontSize="18px" fontWeight="bold" flex={2}>
          Summary
        </Text>
      </Box>
      {topics &&
        topics.map((topic,i) => (
          <SummaryCard
            headline={topic.headline}
            summary={topic.summary}
            handleSeek={(e) => seek(e,i)}
          />
        ))}
    </Box>
  );
};

export default VideoTopics;
