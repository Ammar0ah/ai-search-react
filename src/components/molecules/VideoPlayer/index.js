import { Box, Spinner, Stack } from "@chakra-ui/core";
import React, { useContext, useEffect, useRef, useState } from "react";
import ReactPlayer from "react-player";
import { MobXProviderContext, observer } from "mobx-react";
import * as queryString from "query-string";
import axios from "../../../config/axios";
import cookie from "js-cookie";
import VideoTopics from "../VideoTopics";

const VideoPlayer = () => {
  const { store } = useContext(MobXProviderContext);
  // const { src, playlist, index } = router.query;

  let pass = false;
  const [playing, setPlaying] = useState(true);
  const [_, setReady] = useState(false);
  const [__, setDuration] = useState(0);
  const stop = 20;
  const player = useRef(null);
  const { pl_ind, id } = queryString.parse(window.location.search);


  useEffect(() => {
    if (performance.navigation.type == performance.navigation.TYPE_RELOAD) {
      if (cookie.get("isHome")) store.getHomePlaylist();
      else store.getMyPlaylist();
    }
  }, []);

  const handleSeek = (e,i) => {
    console.log(i,store.getTopics(pl_ind,id))
    pass = true;
    player.current.seekTo(store.getTopics(pl_ind,id)[i].from);
    setPlaying(true);
  };
  const handlProgress = (state) => {
    if (parseInt(state.playedSeconds) === stop && pass) {
      pass = false;
      setPlaying(!player.current.player.isPlaying);
    }
  };
  const handleDuration = (duration) => {
    setDuration(duration);
  };
  console.log(axios.defaults.baseURL + "/video/show/" + id);
  return (
    <Stack size="100%" mt="2em">
      <ReactPlayer
        // light
        ref={player}
        controls
        width="100%"
        height="40em"
        onDuration={handleDuration}
        playing={playing}
        url={[
          {
            src: axios.defaults.baseURL + "/video/show/" + id,
            headers: { Authorization: "Bearer " + cookie.get("token") },
          },
        ]}
        onProgress={handlProgress}
        onReady={() => {
          setReady(true);
        }}
      />
      {store.state === "loading" && <Spinner size="lg"/>}
        <Box>
      
        <VideoTopics topics={store.getTopics(pl_ind, id)} seek={handleSeek} id={id}/>
        </Box>
    </Stack>
  );
};

export default observer(VideoPlayer);
