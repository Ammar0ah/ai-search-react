import { Box, Image } from "@chakra-ui/core";
import React from "react";
import { Link } from "react-router-dom";
import "../../../styles/card.css";
const Card = (props) => {
  const {playlist_index,vid_ind} = props
  return (
    <Link to={`/view?pl_ind=${playlist_index}&id=${vid_ind}`}>
      <Box mb={props.view && 5} className="card">
        <Image src={props.imgUrl} alt={props.alt || "Image"} />
        <Box className="card-content">
          <h2>name: {props.title}</h2>
          <p>description: {props.content}</p>
        </Box>
      </Box>
    </Link>
  );
};
export default Card;
