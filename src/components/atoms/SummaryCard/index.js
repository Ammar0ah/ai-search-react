import { Box, Button, Text } from "@chakra-ui/core";
import React from "react";

const SummaryCard = ({ summary, headline, handleSeek }) => {
  return (
    <Box
      display="flex"
      justifyContent="flex-start"
      borderBottom="#838383 1px solid"
      textAlign="center"
    >
      <Text fontSize="20px" flex={1} borderRight="#838383 1px solid">
        {headline}
      </Text>
      <Text fontSize="18px" flex={2} borderRight="#838383 1px solid">
        {summary}
      </Text>
      <Box flex={0.3} m="auto">
        <Button onClick={handleSeek} >
          Seek
        </Button>
      </Box>
    </Box>
  );
};

export default SummaryCard;
