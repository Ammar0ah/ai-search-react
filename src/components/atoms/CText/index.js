import React from 'react'

const CText = ({weight,size,children}) => {
    return (
        <p style={{fontWeight:weight,fontSize:size}}>
            {children}
        </p>
    )
}

export default CText
