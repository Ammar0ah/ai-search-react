import React from 'react'
import {Button as CButton} from '@chakra-ui/core';
const Button = (props) => {
    return (
        <CButton borderRadius={0} {...props}/>
    )
}

export default Button
