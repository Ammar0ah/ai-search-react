import React from "react";
import {
  Button,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Input,
  InputGroup,
  InputRightElement,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Scale,
  useDisclosure,
  useToast,
} from "@chakra-ui/core";
import * as Yup from "yup";
import { useFormik } from "formik";
import { MobXProviderContext, observer } from "mobx-react";
import { useContext, useState } from "react";

const Login = () => {
  const { store } = useContext(MobXProviderContext);

  const [loading, setLoading] = useState(false);
  const toast = useToast();
  const [show, setShow] = React.useState(false);

  const handleClick = () => setShow(!show);
  
  const validationSchema = Yup.object().shape({
    email: Yup.string().email("Invalid email").required("Email is required"),
    password: Yup.string()
      .min(8, "Too Short!")
      .max(50, "Too Long!")
      .required("Password is required"),
  });

  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    onSubmit: (values) => {
      store.reqAuth("login", values, null, null, () => {
        setLoading(false);
        toast({
          title: store.state.toUpperCase(),
          description: store.message,
          duration: 2000,
          position: "top-right",
          status: store.state,
          isClosable: true,
        });
      });
    },
    validationSchema,
  });

  const {
    errors,
    touched,
    handleBlur,
    values,
    handleChange,
    handleSubmit,
  } = formik;

  const { isOpen, onOpen, onClose } = useDisclosure();

  const initialRef = React.useRef();
  const finalRef = React.useRef();

  return (
    <>
      <Button
        rightIcon="arrow-forward"
        variant="solid"
        variantColor="black"
        backgroundColor="#323232"
        size="sm"
        mb={1}
        onClick={onOpen}
        fontWeight="normal"
      >
        Login
      </Button>
      <Scale in={isOpen}>
        {(styles) => (
          <Modal
            initialFocusRef={initialRef}
            finalFocusRef={finalRef}
            isOpen={isOpen}
            onClose={onClose}
          >
            <ModalOverlay opacity={styles.opacity} />
            <ModalContent {...styles}>
              <form onSubmit={handleSubmit}>
                <ModalHeader>Login to your account</ModalHeader>
                <ModalCloseButton />
                <ModalBody pb={6}>
                  <FormControl mt={4} isInvalid={errors.email && touched.email}>
                    <FormLabel>Email</FormLabel>
                    <Input
                      name="email"
                      placeholder="Email"
                      onChange={handleChange}
                      value={values.email}
                      onBlur={handleBlur}
                    />

                    {touched.email && errors.email && (
                      <FormErrorMessage>{errors.email}</FormErrorMessage>
                    )}
                  </FormControl>

                  <FormControl
                    mt={4}
                    isInvalid={errors.password && touched.password}
                  >
                    <FormLabel>Password </FormLabel>

                    <InputGroup size="md">
                      <Input
                        pr="4.5rem"
                        type={show ? "text" : "password"}
                        placeholder="Enter password"
                        name="password"
                        onChange={handleChange}
                        value={values.password}
                        onBlur={handleBlur}
                      />
                      <InputRightElement width="4.5rem">
                        <Button h="1.75rem" size="sm" onClick={handleClick}>
                          {show ? "Hide" : "Show"}
                        </Button>
                      </InputRightElement>
                    </InputGroup>
                    {touched.password && errors.password && (
                      <FormErrorMessage>{errors.password}</FormErrorMessage>
                    )}
                  </FormControl>
                </ModalBody>

                <ModalFooter>
                  <Button
                    type="submit"
                    variantColor="red"
                    isLoading={loading}
                    isDisabled={Object.keys(errors).length > 0}
                    mr={3}
                  >
                    Login
                  </Button>
                  <Button onClick={onClose} variant="outline">
                    Cancel
                  </Button>
                </ModalFooter>
              </form>
            </ModalContent>
          </Modal>
        )}
      </Scale>
    </>
  );
};

export default observer(Login);
