import React, { useEffect } from "react";
import { inject, observer } from "mobx-react";
import {
  Box,
  Button,
  Flex,
  FormControl,
  Grid,
  Icon,
  Image,
  Input,
  InputGroup,
  InputRightElement,
  Link as ChakraLink,
  Spinner,
  Stack,
  useToast,
} from "@chakra-ui/core";
import Login from "../Login";
import Signup from "../Signup";
import CText from "../../atoms/CText";
import { Link, useHistory } from "react-router-dom";

const NavBar = inject("store")(
  observer(({ store }) => {
    // const {name} = useStore()
    // const router = useRouter();
    const toast = useToast();
    useEffect(() => {
      store.getUser(null, () =>
        toast({
          title: store.message,
          status: store.state,
          position: "top-right",
        })
      );
    }, []);
    const history = useHistory();
    const onLogout = () => {
      store.logout();
      history.replace("/");
    };

    const goToMyPlaylist = () => {
      history.push("my-playlists");
    };
    console.log(history.location.pathname);
    return (
      <Flex
        p={2}
        height={70}
        width="100%"
        // borderBottom="1px #252525 solid"
        top={0}
        zIndex={10}
        pos="fixed"
        background="#f4f4f4"
        boxShadow="0 10px 35px rgba(50,50,93,.1),0 2px 15px rgba(0,0,0,.07)"
      >
        <Box marginLeft={"2em"} marginRight={"1em"}>
          <ChakraLink href="/">
            <Image
              rounded="full"
              size="3.5em"
              src={require("./logo.jpg")}
              alt="Segun Adebayo"
            />
          </ChakraLink>
        </Box>
        <FormControl m="auto" w="55%">
          <InputGroup>
            <Input id="fname" placeholder="Search..." />
            <InputRightElement children={<Icon name="search" />} />
          </InputGroup>
        </FormControl>
        {store.isAuthorized ? (
          <Grid
            dir="row"
            spacing={10}
            display="flex"
            width={{ md: "23%" }}
            justifyContent="space-between"
            alignItems="center"
            ml={{ md: "1em", sm: 0 }}
          >
            {store.state === "success" ? (
              <CText>Hello, {store.name}</CText>
            ) : (
              <Spinner size="xs" />
            )}
            {history.location.pathname === "/my-playlists" ? (
              <Button
                float="right"
                rightIcon="add"
                variant="outline"
                borderColor="#006a71"
                color="#006a71"
                size="sm"
                fontWeight="normal"
                mr={4}
                onClick={() => history.push("/add-playlist")}
              >
                Add Playlist
              </Button>
            ) : (
              <Link to="/my-playlists">
                <Button
                  float="right"
                  rightIcon="drag-handle"
                  variant="outline"
                  variantColor="#f95278"
                  size="sm"
                  fontWeight="normal"
                  mr={4}
                  onClick={goToMyPlaylist}
                >
                  My Playlists
                </Button>
              </Link>
            )}
            <Button
              float="right"
              rightIcon="arrow-forward"
              variant="outline"
              variantColor="#f95278"
              size="sm"
              fontWeight="normal"
              mr={4}
              onClick={onLogout}
            >
              Logout
            </Button>
          </Grid>
        ) : (
          <Stack
            spacing={40}
            mr={{ md: "3em", sm: 0 }}
            ml={{ md: "1em", sm: 0 }}
          >
            <Login />
            <Signup />
          </Stack>
        )}
      </Flex>
    );
  })
);

export default NavBar;
