import { Box, Button } from "@chakra-ui/core";
import { MobXProviderContext, observer } from "mobx-react";
import React, { useContext } from "react";
import { useHistory } from "react-router-dom";
import CardContainer from "../../../containers/CardContainer";

const UserPlaylists = () => {
  const { store } = useContext(MobXProviderContext);
  const history = useHistory();

  return (
    <Box w="100%">
      {store.playlists.map((playlist, i) => {
        return (
          <>
            <Button
              float="right"
              rightIcon="arrow-up"
              variant="outline"
              variantColor="black"
              size="sm"
              fontWeight="normal"
              mr={4}
              onClick={() => history.push(`/upload-video/${playlist.id}`)}
            >
              Upload
            </Button>
            <CardContainer
              name={playlist.name}
              pl_ind={i}
              videos={playlist.videos}
            />
          </>
        );
      })}
    </Box>
  );
};

export default observer(UserPlaylists);
