import {
  Box,
  Button,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Image,
  Input,
  InputGroup,
  ModalBody,
  ModalFooter,
  ModalHeader,
  useDisclosure,
  useToast,
} from "@chakra-ui/core";
import React, { useContext, useState } from "react";
import * as Yup from "yup";
import { useFormik } from "formik";
import { MobXProviderContext, observer } from "mobx-react";
import { useHistory } from "react-router-dom";

/** Add a new Playlist */

const AddPlaylist = () => {
  const { store } = useContext(MobXProviderContext);
  
  const [img, setImg] = useState(null);
  const [loading, setLoading] = useState(false);
  const toast = useToast();
  const history = useHistory();
  const validationSchema = Yup.object().shape({
    name: Yup.string().min(2).max(50).required("Video name is required!"),

    description: Yup.string()
      .min(5)
      .max(255)
      .required("Please write a video description!"),
  });

  const toastAction = () => {
    toast({
      title: store.state.toUpperCase(),
      description: store.message,
      duration: 1500,
      position: "top-right",
      status: store.state,
      isClosable: true,
    });
  };

  const formik = useFormik({
    initialValues: {
      name: "My Favorite",
      cover: {},
      description: "My Favorite Playlist",
    },
    onSubmit: (values) => {
      if (formik.isValid) {
        setLoading(true);
        const formData = new FormData();
        formData.append("name", values.name);
        formData.append("description", values.description);
        formData.append("cover", values.cover);
        store.addPlaylist(
          formData,
          // on success
          () => {
            toastAction();
            history.push("/");
            setLoading(false);
          },
          // on fail
          () => {
            setLoading(false);
            toastAction();
          }
        );
      }
    },
    validationSchema,
  });

  const {
    errors,
    touched,
    handleBlur,
    values,
    handleChange,
    handleSubmit,
    setFieldValue,
  } = formik;

  const {  onClose } = useDisclosure();
    return (
    <Box
      width="50%"
      m="auto"
      mt={{ md: "7em", sm: "3em" }}
      boxShadow="0 1px 1rem -4px #000"
    >
      <form onSubmit={handleSubmit}>
        <ModalHeader>Add a new playlist</ModalHeader>
        <ModalBody pb={6}>
          <FormControl mt={4} isInvalid={errors.name && touched.name}>
            <FormLabel>Playlist Name</FormLabel>
            <Input
              name="name"
              placeholder="Name"
              onChange={handleChange}
              value={values.name}
              onBlur={handleBlur}
            />

            {touched.name && errors.name && (
              <FormErrorMessage>{errors.name}</FormErrorMessage>
            )}
          </FormControl>

          <FormControl
            mt={4}
            isInvalid={errors.description && touched.description}
          >
            <FormLabel>Description </FormLabel>
            <InputGroup size="md">
              <Input
                pr="4.5rem"
                type="text"
                placeholder="Please add Playlist description"
                name="description"
                onChange={handleChange}
                value={values.description}
                onBlur={handleBlur}
              />
            </InputGroup>
            {touched.description && errors.description && (
              <FormErrorMessage>{errors.description}</FormErrorMessage>
            )}

            <FormLabel>Cover Image </FormLabel>
            <InputGroup size="md">
              <Input
                pt="0.2em"
                type="file"
                placeholder="Please add Playlist cover "
                name="file"
                // dir="rtl"
                onChange={(event) => {
                  setFieldValue("cover", event.currentTarget.files[0]);
                  let reader = new FileReader();
                  reader.onloadend = () => setImg(reader.result);
                  reader.readAsDataURL(event.currentTarget.files[0]);
                }}
                // value={values.cover}
                onBlur={handleBlur}
              />
            </InputGroup>
            {img && <Image src={img} w={200} h={200} m="auto" mt="1em" />}
            {touched.cover && errors.cover && (
              <FormErrorMessage>{errors.cover}</FormErrorMessage>
            )}
          </FormControl>
        </ModalBody>

        <ModalFooter>
          <Button
            type="submit"
            variantColor="red"
            isLoading={loading}
            isDisabled={Object.keys(errors).length > 0}
            mr={3}
          >
            Add
          </Button>
          <Button onClick={onClose} variant="outline">
            Cancel
          </Button>
        </ModalFooter>
      </form>
    </Box>
  );
};

export default observer(AddPlaylist);
