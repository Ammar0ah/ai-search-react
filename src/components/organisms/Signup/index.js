import React from "react";
import {
  Button,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Input,
  InputGroup,
  InputRightElement,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Scale,
  useDisclosure,
  useToast,
} from "@chakra-ui/core";
import {  MobXProviderContext, observer } from "mobx-react";
import { useFormik } from "formik";
import * as Yup from "yup";
import { useContext, useState } from "react";

const Signup = () => {
  const [loading, setLoading] = useState(false);

  const { store } = useContext(MobXProviderContext);

  const toast = useToast();

  const validationSchema = Yup.object().shape({
    name: Yup.string()
      .min(2, "Too Short!")
      .max(50, "Too Long!")
      .required("name is required"),
    email: Yup.string().email("Invalid email").required("Email is required"),
    password: Yup.string()
      .min(8, "Too Short!")
      .max(50, "Too Long!")
      .required("Password is required"),
    password_confirmation: Yup.string().oneOf(
      [Yup.ref("password"), null],
      "Passwords must match"
    ),
  });
  const formik = useFormik({
    initialValues: {
      email: "ammar@h.com",
      name: "ammar",
      password: "123123123",
      password_confirmation: "123123123",
    },
    onSubmit: (values) => {
      if (formik.isValid) {
        setLoading(true);
        store.reqAuth("signup", values, null, null, () => {
          setLoading(false);
          toast({
            title: store.state.toUpperCase(),
            description: store.message,
            duration: 2000,
            position: "top-right",
            status: store.state,
            isClosable: true,
          });
        });
      }
    },
    validationSchema,
  });
  const {
    errors,
    touched,
    handleBlur,
    values,
    handleChange,
    handleSubmit,
  } = formik;
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [show, setShow] = React.useState(false);

  const initialRef = React.useRef();
  const finalRef = React.useRef();

  const handleClick = () => setShow(!show);

  return (
    <>
      <Button
        rightIcon="plus-square"
        variant="outline"
        variantColor="red"
        size="sm"
        fontWeight="normal"
        onClick={onOpen}
      >
        Signup
      </Button>
      <Scale in={isOpen}>
        {(styles) => (
          <Modal
            initialFocusRef={initialRef}
            finalFocusRef={finalRef}
            isOpen={isOpen}
            onClose={onClose}
          >
            <ModalOverlay opacity={styles.opacity} />

            <ModalContent {...styles}>
              <form onSubmit={handleSubmit}>
                <ModalHeader>Create your account</ModalHeader>
                <ModalCloseButton />
                <ModalBody pb={6}>
                  <FormControl isInvalid={errors.name && touched.name}>
                    <FormLabel htmlFor="name">Name</FormLabel>
                    <Input
                      ref={initialRef}
                      placeholder="Name"
                      name="name"
                      onChange={handleChange}
                      value={values.name}
                      onBlur={handleBlur}
                    />

                    {touched.name && errors.name && (
                      <FormErrorMessage>{errors.name}</FormErrorMessage>
                    )}
                  </FormControl>

                  <FormControl mt={4} isInvalid={errors.email && touched.email}>
                    <FormLabel>Email</FormLabel>
                    <Input
                      name="email"
                      placeholder="Email"
                      onChange={handleChange}
                      value={values.email}
                      onBlur={handleBlur}
                    />

                    {touched.email && errors.email && (
                      <FormErrorMessage>{errors.email}</FormErrorMessage>
                    )}
                  </FormControl>

                  <FormControl
                    mt={4}
                    isInvalid={errors.password && touched.password}
                  >
                    <FormLabel>Password </FormLabel>

                    <InputGroup size="md">
                      <Input
                        pr="4.5rem"
                        type={show ? "text" : "password"}
                        placeholder="Enter password"
                        name="password"
                        onChange={handleChange}
                        value={values.password}
                        onBlur={handleBlur}
                      />
                      <InputRightElement width="4.5rem">
                        <Button h="1.75rem" size="sm" onClick={handleClick}>
                          {show ? "Hide" : "Show"}
                        </Button>
                      </InputRightElement>
                    </InputGroup>
                    {touched.password && errors.password && (
                      <FormErrorMessage>{errors.password}</FormErrorMessage>
                    )}
                  </FormControl>

                  <FormControl
                    mt={4}
                    isInvalid={
                      errors.password_confirmation &&
                      touched.password_confirmation
                    }
                  >
                    <FormLabel>Confirm Password </FormLabel>

                    <InputGroup size="md">
                      <Input
                        pr="4.5rem"
                        type={show ? "text" : "password"}
                        placeholder="Confirmation"
                        name="password_confirmation"
                        onChange={handleChange}
                        value={values.password_confirmation}
                        onBlur={handleBlur}
                      />
                      <InputRightElement width="4.5rem">
                        <Button h="1.75rem" size="sm" onClick={handleClick}>
                          {show ? "Hide" : "Show"}
                        </Button>
                      </InputRightElement>
                    </InputGroup>
                    {touched.password_confirmation &&
                      errors.password_confirmation && (
                        <FormErrorMessage>
                          {errors.password_confirmation}
                        </FormErrorMessage>
                      )}
                  </FormControl>
                </ModalBody>

                <ModalFooter>
                  <Button
                    type="submit"
                    variantColor="red"
                    isLoading={loading}
                    mr={3}
                    isDisabled={Object.keys(errors).length > 0}
                  >
                    Create
                  </Button>
                  <Button onClick={onClose} variant="outline">
                    Cancel
                  </Button>
                </ModalFooter>
              </form>
            </ModalContent>
          </Modal>
        )}
      </Scale>
    </>
  );
};

export default observer(Signup);
