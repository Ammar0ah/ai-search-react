import React, { useContext, useEffect } from "react";
import { MobXProviderContext, observer } from "mobx-react";
import {
  SimpleGrid,
} from "@chakra-ui/core";
import Card from "../../molecules/Card";
import { isObject } from "lodash";
import axios from '../../../config/axios'


const HomeVideos = () => {
  const { store } = useContext(MobXProviderContext);

  useEffect(() => {
    store.getHomePlaylist();
   
  }, []);

  return (
    <SimpleGrid mt={5} columns={{ md: 4, sm: 1 }} w="100%" spacing={5}>
      {store.defaultPlaylist.map((video, i) => {
        return (
          <>
            {isObject(video) && (
              <Card
                title={video.title}
                path={video.video_path}
                playlist_index={i}
                vid_ind={video.id}
                content={video.description}
                imgUrl={`${axios.defaults.baseURL}/video/thumbnail/${
                  video.id
                }`}
              />
            )}
          </>
        );
      })}
      {/* <video /> */}
    </SimpleGrid>
  );
};

export default observer(HomeVideos);
