import {
  Box,
  Button,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Image,
  Input,
  InputGroup,
  ModalBody,
  ModalFooter,
  ModalHeader,
  useDisclosure,
} from "@chakra-ui/core";
import React, {  useState } from "react";
import * as Yup from "yup";
import { useFormik } from "formik";
import {  observer } from "mobx-react";
import { useParams } from "react-router-dom";

const AddPlaylist = () => {
  const { id } = useParams();
  const [img, setImg] = useState(null);
  const [loading] = useState(false);
  const validationSchema = Yup.object().shape({
    title: Yup.string().min(2).max(50).required("Video title is required!"),

    description: Yup.string()
      .min(5)
      .max(255)
      .required("Please write a video description!"),
  });

  const formik = useFormik({
    initialValues: {
      title: "My Favorite",
      cover: {},
      description: "My Favorite Playlist",
    },
    onSubmit: (values) => {
      if (formik.isValid) {
        console.log(values);
        const formdata = new FormData();
        formdata.append("title", values.title);
        formdata.append("description", values.description);
        formdata.append("cover", values.cover);
        formdata.append("play_list_id", id);
 
      }
    },
    validationSchema,
  });

  const {
    errors,
    touched,
    handleBlur,
    values,
    handleChange,
    handleSubmit,
    setFieldValue,
  } = formik;

  const {  onClose } = useDisclosure();
  return (
    <Box
      width="50%"
      m="auto"
      mt={{ md: "7em", sm: "3em" }}
      boxShadow="0 1px 1rem -4px #000"
    >
      <form onSubmit={handleSubmit}>
        <ModalHeader>Add a new Video</ModalHeader>
        <ModalBody pb={6}>
          <FormControl mt={4} isInvalid={errors.title && touched.title}>
            <FormLabel>Video Title</FormLabel>
            <Input
              name="title"
              placeholder="Title..."
              onChange={handleChange}
              value={values.title}
              onBlur={handleBlur}
            />

            {touched.title && errors.title && (
              <FormErrorMessage>{errors.title}</FormErrorMessage>
            )}
          </FormControl>

          <FormControl
            mt={4}
            isInvalid={errors.description && touched.description}
          >
            <FormLabel>Description </FormLabel>
            <InputGroup size="md">
              <Input
                pr="4.5rem"
                type="text"
                placeholder="Please add Playlist description"
                name="description"
                onChange={handleChange}
                value={values.description}
                onBlur={handleBlur}
              />
            </InputGroup>
            {touched.description && errors.description && (
              <FormErrorMessage>{errors.description}</FormErrorMessage>
            )}

            <FormLabel>Cover Image </FormLabel>
            <InputGroup size="md">
              <Input
                pt="0.2em"
                type="file"
                placeholder="Please add Playlist cover "
                name="file"
                // dir="rtl"
                onChange={(event) => {
                  setFieldValue("cover", event.currentTarget.files[0]);
                  let reader = new FileReader();
                  reader.onloadend = () => setImg(reader.result);
                  reader.readAsDataURL(event.currentTarget.files[0]);
                }}
                // value={values.cover}
                onBlur={handleBlur}
              />
            </InputGroup>
            {img && <Image src={img} w={200} h={200} m="auto" mt="1em" />}
            {touched.cover && errors.cover && (
              <FormErrorMessage>{errors.cover}</FormErrorMessage>
            )}
          </FormControl>
        </ModalBody>

        <ModalFooter>
          <Button
            type="submit"
            variantColor="red"
            isLoading={loading}
            isDisabled={Object.keys(errors).length > 0}
            mr={3}
          >
            Add
          </Button>
          <Button onClick={onClose} variant="outline">
            Cancel
          </Button>
        </ModalFooter>
      </form>
    </Box>
  );
};

export default observer(AddPlaylist);
