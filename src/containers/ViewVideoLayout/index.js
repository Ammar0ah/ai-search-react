import { Box, Grid } from "@chakra-ui/core";
import { MobXProviderContext, observer } from "mobx-react";
import React, { useContext, useEffect, useState } from "react";
import VideoPlayer from "../../components/molecules/VideoPlayer";
import * as queryString from "query-string";

const ViewVideoLayout = () => {
  const { store } = useContext(MobXProviderContext);
  const { pl_ind } = queryString.parse(window.location.search);
  // console.log(store.playlists[pl_ind],pl_ind)
  const [_, setPlaylist] = useState([]);
  useEffect(() => {
    setPlaylist(store.playlists[pl_ind]);
  }, [store.playlists[pl_ind]?.videos]);
 
  return (
    <Grid
      m="auto"
      px="10em">
      <Box width="100%" height="100%" margin="4em auto 2em auto" boxShadow="0 4px 2rem -4px #000" backgroundColor="#ccc">
        <Box h="60%">
          <VideoPlayer />
        </Box>
        
      </Box>
    </Grid>
  );
};

export default observer(ViewVideoLayout);
