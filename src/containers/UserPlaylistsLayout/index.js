import { Box, Button, Grid, Spinner, Stack, useToast } from "@chakra-ui/core";
import { MobXProviderContext, observer } from "mobx-react";
import React, { useContext, useEffect } from "react";
import { useHistory } from "react-router-dom";
import UserPlaylists from "../../components/organisms/UserPlaylists";

const UserPlaylistLayout = () => {
  const { store } = useContext(MobXProviderContext);
  const toast = useToast();
  const history = useHistory();
  useEffect(() => {
    store.setHome(false)
  },[store.defaultPlaylist])
  useEffect(() => {
    store.setState("loading");
    store.setHome(false)
    store.getMyPlaylist(() =>
      toast({
        title: store.message,
        status: store.state,
        position: "top-right",
        isClosable: true,
      })
    );
  }, []);
  return (
    <Box mt="4em">
      <Grid
        templateColumns="repeat(1, 1fr)"
        w="100%"
        mt="1.5em"
        px="2em"
        gap={1}
        float="left"
      >
        {store.state === "loading" && <Spinner size="lg" />}

        <UserPlaylists />
      </Grid>
    </Box>
  );
};

export default observer(UserPlaylistLayout);
