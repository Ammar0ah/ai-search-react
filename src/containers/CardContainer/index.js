import React from "react";
import "../../styles/card.css";
import Card from "../../components/molecules/Card";
import { Grid } from "@chakra-ui/core";
import { isObject } from "lodash";
import CText from "../../components/atoms/CText";
import axios from '../../config/axios'

/** single playlist */

const CardContainer = ({ videos,pl_ind,pl_id, name, view }) => {
  return (
    <>
      <CText weight="500" size={25}>
        {name}
      </CText>

      <Grid
        rowGap={50}
        h="100%"
        display={view && "block"}
        className="cards-container"
        >
        {videos &&
          videos.map(
            (video, i) =>
            isObject(video)  && (
              <>
                <Card
                title={video.title}
                  path={video.video_path}
                  playlist_index={pl_ind}
                  playlist_id={pl_id}
                  vid_ind={video.id}
                  content={video.description}
                  imgUrl={`${axios.defaults.baseURL}/video/thumbnail/${
                    video.id
                  }`}
                />
                </>
              )
          )}
      </Grid>
    </>
  );
};
export default CardContainer;
