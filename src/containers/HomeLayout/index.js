import React, { useContext, useEffect } from "react";
import { MobXProviderContext, observer } from "mobx-react";
import {
  Grid,
  Spinner,
} from "@chakra-ui/core";
import CText from "../../components/atoms/CText";
import HomeVideos from "../../components/organisms/HomeVideos";

/** Home Page */

const HomeLayout = () => {
  const { store } = useContext(MobXProviderContext);

  useEffect(() => {
    store.setState("loading");
    store.getHomePlaylist();
    // return () => {
      store.setHome(true)
    //   store.setHome(false)
    // }
      // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [store.isAuthorized]);
  useEffect(() => {
    store.setHome(true)
  },[])

  return (
    <>
      {store.state === "loading" && <Spinner size="lg" />}

      <Grid templateColumns="repeat(1, 1fr)" gap={1} float="left" px="2em">
        <>
          <CText weight="500" size={25}>
            Home
          </CText>
          <HomeVideos />
        </>
      </Grid>
    </>
  );
};

export default observer(HomeLayout);
